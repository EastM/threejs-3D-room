import { useEffect, useRef } from 'react'
import logo from './logo.svg'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import back from './assets/back.png'
import bottom from './assets/bottom.png'
import front from './assets/front.png'
import left from './assets/left.png'
import right from './assets/right.png'
import top from './assets/top.png'

import './App.css'

function App() {

  const ref = useRef<any>();

  const textureLoader = (img: any) => {
    const loader = new THREE.TextureLoader();
    const texture = loader.load(img);
    const material = new THREE.MeshBasicMaterial({
      map: texture,
      side: THREE.DoubleSide
    });
    return material;
  }

  useEffect(() => {
    const camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 10);
    camera.position.z = 1;

    const scene = new THREE.Scene();

    const arr = [
      textureLoader(back),
      textureLoader(front),
      textureLoader(top),
      textureLoader(bottom),
      textureLoader(left),
      textureLoader(right),
    ]

    const geometry = new THREE.BoxGeometry(4, 4, 4);
    const material = new THREE.MultiMaterial(arr);



    const mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);

    // 坐标系辅助线
    const axis = new THREE.AxesHelper();
    scene.add(axis);

    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setAnimationLoop(animation);
    ref.current.appendChild(renderer.domElement);

    // animation

    function animation(time: any) {

      // mesh.rotation.x = time / 2000;
      // mesh.rotation.y = time / 1000;

      renderer.render(scene, camera);

    }
    const oribit = new OrbitControls(camera, renderer.domElement);
    oribit.enableZoom = false;
  }, [])




  return (
    <div ref={ref} className="box">

    </div>
  )
}

export default App
